﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour
{

    public GameObject Wall;
    public int WallCount = 10;
    public float InitGap = 3f;
    public float MinHeight = 1f;

    [HideInInspector]
    public float speed;
    public float initSpeed = 3f;
    public float maxSpeed = 10f;

    public Color[] colors;

    public Player player;

    private GameObject[] walls;
    private Transform fragments;

    public GameObject scripts;
    private NetworkScript script;

    public bool passedWall = false;

    public void endGame()
    {
        speed = 0;
        player.endGame();
    }

    public void init()
    {
        for (int i = 0; i < WallCount; i++)
        {
            Destroy(walls[i]);
            walls[i] = Instantiate(Wall);
            walls[i].transform.parent = this.transform;
            walls[i].transform.position = new Vector3(InitGap * i + transform.position.x, 0);
            walls[i].GetComponent<Wall>().passed = false;
        }

        Wall wallScript = Wall.GetComponent<Wall>();
        wallScript.ressetPassed();
    }

    void Start()
    {
        speed = initSpeed;

        walls = new GameObject[WallCount];

        init();

        fragments = this.transform.Find("Fragments");

        script = scripts.GetComponent<NetworkScript>();
    }

    public void writeSocket(string text)
    {
        script.writeSocket(text);
    }

    void Update()
    {
        int i;
        for (i = 0; i < fragments.childCount; i++)
        {
            Transform fragment = fragments.GetChild(i);
            fragment.transform.localScale -= new Vector3(0.5f * Time.deltaTime, 0.5f * Time.deltaTime, 0.5f * Time.deltaTime);
            fragment.transform.Translate(new Vector3(-speed * Time.deltaTime, 0, 0));
            Vector3 newScale = fragment.transform.localScale;
            if (newScale.x <= 0 || newScale.y <= 0 || newScale.z <= 0)
            {
                Destroy(fragment.gameObject);
            }
        }



        float nearestDistance = 999;
        int nearestIndex = -1;
        i = 0;
        foreach (GameObject wall in walls)
        {
            float distance = wall.GetComponent<Wall>().transform.position.x - player.GetComponent<Player>().transform.position.x + wall.GetComponent<Wall>().transform.localScale.x / 2 + player.GetComponent<Player>().transform.localScale.x / 2;

            if (distance < nearestDistance && distance > 0)
            {
                nearestIndex = i;
                nearestDistance = distance;
            }
            i++;
        }

        if (nearestIndex != -1)
        {
            Transform topWall = walls[nearestIndex].transform.Find("topWall");
            float topGap = topWall.position.y - topWall.localScale.y / 2 - player.GetComponent<Player>().transform.localScale.y / 2;

            Transform botWall = walls[nearestIndex].transform.Find("botWall");
            float botGap = botWall.position.y + botWall.localScale.y / 2 + player.GetComponent<Player>().transform.localScale.y / 2 + 1f;

            Transform tmp = walls[nearestIndex].transform.Find("target_topWall");
            Transform target = null;
            if (tmp != null)
            {
                target = tmp;
            }
            tmp = walls[nearestIndex].transform.Find("target_botWall");
            if (tmp != null)
            {
                target = tmp;
            }

            if (target == null)
                return;

            string pw = "";
            if (passedWall)
            {
                pw = ", \"passedWall\":" + "\"true\"";
                passedWall = false;
            }
            string playerCoords = ",\"X\":" + "\"" + player.transform.position.x + "\"" + " , \"Y\":" + "\"" + (player.transform.position.y) + "\", \"vY\":" + "\"" + (player.velocity.y) + "\"";
            float topDistance = Mathf.Sqrt(nearestDistance*nearestDistance + Mathf.Pow(topGap - player.transform.position.x, 2));
            float botDistance = Mathf.Sqrt(nearestDistance * nearestDistance + Mathf.Pow(botGap - player.transform.position.x, 2));
            float targetDistance = Mathf.Sqrt(nearestDistance * nearestDistance + Mathf.Pow(topGap - player.transform.position.x, 2));
            float midDistance = Mathf.Sqrt(nearestDistance * nearestDistance + Mathf.Pow(topGap - (topGap - botGap)/2f - player.transform.position.x, 2));
            float targetYDistance = player.transform.position.y - target.position.y;
            float midYDistance = player.transform.position.y - (topGap - (topGap - botGap) / 2f);
            script.writeSocket("{\"targetYDistance\":" + "\"" + targetYDistance + "\"" + " , \"midYDistance\":" + "\"" + midYDistance + "\"" + " , \"distance\":" + "\"" + nearestDistance + "\"" + ", \"vY\":" + "\"" + player.velocity.y + "\"" + pw +"}");
            Debug.Log("{\"targetYDistance\":" + "\"" + targetYDistance + "\"" + " , \"midYDistance\":" + "\"" + midYDistance + "\"" + " , \"distance\":" + "\"" + nearestDistance + "\"" + ", \"vY\":" + "\"" + player.velocity.y + "\"" + pw + "}");
        }
    }
}

