﻿using System.Collections;
using UnityEngine;

public class OnCollision : MonoBehaviour {

    public GameObject walls;
    public bool showAnimation;

	void OnCollisionEnter(Collision collision)
	{
        if (GetComponent<Player>().end)
            return;

        Player p = this.gameObject.GetComponent<Player>();

        if (collision.gameObject.name == "target_topWall" || collision.gameObject.name == "target_botWall")
        {
            Transform hitWall = collision.gameObject.transform.parent.Find(collision.gameObject.name.Substring(collision.gameObject.name.IndexOf("_")+1));

			Vector3 position = hitWall.gameObject.transform.position;
			Vector3 size = hitWall.transform.localScale;
			Color color = hitWall.gameObject.GetComponent<Renderer> ().material.color;

            Destroy(collision.gameObject);
            hitWall.gameObject.SetActive(false);

            if (p.dashing) {
                GetComponent<Player>().mana += 2;
                walls.GetComponent<Walls>().writeSocket("{\"dashTarget\":" + "\"true\"}");
                Debug.Log("BOOM");
                Transform fragments = hitWall.gameObject.transform.parent.parent.Find ("Fragments");
				int nX = (int)Mathf.Ceil (size.x);
				int nY = (int)Mathf.Ceil (size.y);
				int nZ = (int)Mathf.Ceil (size.z);

#if UNITY_STANDALONE || UNITY_WEBPLAYER

				nX = nX % 2 == 0 ? nX + 1 : nX;
				nY = nY % 2 == 0 ? nY + 1 : nY;
				nZ = nZ % 2 == 0 ? nZ + 1 : nZ;

#else
                nX = nX % 2 == 0 ? nX - 1 : nX;
                nY = nY % 2 == 0 ? nY - 1 : nY;
                nZ = nZ % 2 == 0 ? nZ - 1 : nZ;
#endif
                if (showAnimation)
                {
                    for (int i = -nX / 2; i <= nX / 2; i++)
                    {
                        for (int j = -nY / 2; j <= nY / 2; j++)
                        {
                            for (int k = -nZ / 2; k <= nZ / 2; k++)
                            {
                                GameObject fragment = GameObject.CreatePrimitive(PrimitiveType.Cube);

                                fragment.transform.localScale = new Vector3(size.x / nX, size.y / nY, size.z / nZ);
                                fragment.transform.position = new Vector3(position.x + i * size.x / nX, position.y + i * size.y / nY, position.z + i * size.z / nZ);
                                fragment.name = "fragment";

                                fragment.GetComponent<Renderer>().material.color = color;
                                Rigidbody rb = fragment.AddComponent<Rigidbody>();
                                rb.mass = 0.001f;
                                rb.useGravity = false;
                                rb.AddForce(new Vector3(5f, 0, 0));

                                fragment.transform.parent = fragments;

                            }
                        }
                    }
                }
			}
            else
            {
                GetComponent<Player>().endGame();
            }
		}
        else if (collision.gameObject.name == "topWall" || collision.gameObject.name == "botWall")
        {
            if(!p.dashing)
                GetComponent<Player>().endGame();
        }
    }
}
