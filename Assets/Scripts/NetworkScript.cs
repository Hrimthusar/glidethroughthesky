﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Net.Sockets;



public class NetworkScript : MonoBehaviour
{
    public String host = "localhost";
    public Int32 port = 50000;

    public GameObject playerObject;
    public GameObject settings;
    private Player player;
    private float gravity;

    internal Boolean socket_ready = false;
    TcpClient tcp_socket;
    NetworkStream net_stream;

    StreamWriter socket_writer;
    StreamReader socket_reader;

    [HideInInspector]
    public bool AIjump = false;
    public bool AIdash = false;

    public bool training = false;

    //void FixedUpdate()
    void Update()
    {
        string received_data = readSocket();
        string coords = "{\"X\":" + "\"" + playerObject.transform.position.x + "\"" + " , \"Y\":" + "\"" + (playerObject.transform.position.y)  + "\"}";
        writeSocket(coords);

        string vY = "{\"vY\":" + "\"" + playerObject.GetComponent<Rigidbody>().velocity.y + "\"}";
        writeSocket(vY);

        if (received_data != "")
        {
            if (received_data == "jump")
                AIjump = true;
            if (received_data == "dash")
                AIdash = true;
        }
    }

    void OnApplicationQuit()
    {
        closeSocket();
    }

    public void setupSocket()
    {
        try
        {
            training = true;
            tcp_socket = new TcpClient(host, port);

            net_stream = tcp_socket.GetStream();
            socket_writer = new StreamWriter(net_stream);
            socket_reader = new StreamReader(net_stream);

            socket_ready = true;
        }
        catch (Exception e)
        {
            Debug.Log("Socket error: " + e);
        }
    }

    public void writeSocket(string line)
    {
        if (!socket_ready)
            return;

        line = line + "\r\n";
        socket_writer.Write(line);
        socket_writer.Flush();
    }

    public String readSocket()
    {
        if (!socket_ready)
            return "";

        if (net_stream.DataAvailable)
            return socket_reader.ReadLine();

        return "";
    }

    public void closeSocket()
    {
        if (!socket_ready)
            return;

        socket_writer.Close();
        socket_reader.Close();
        tcp_socket.Close();
        socket_ready = false;
    }
}