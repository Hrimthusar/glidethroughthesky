﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public float jumpPower = 20f;
	public float maxSpeed = 25f;

	public Color[] colors;

	public GameObject settingsObject;
	public GameObject bounds;
	public GameObject walls;
    public GameObject AI;

    [HideInInspector]
    public NetworkScript commands;

    private bool win = false;

    private float top;
	private float bot;

    public bool enableInput = false;

    public int mana;
    public Color currColor;

    [HideInInspector]
	public bool dashing = false;
	private float dashTimeLeft;
	public float dashLength;
	public float dashSpeedIncrease;

    [HideInInspector]
    public bool end = false;
    public float endAnimationLength;
    private float endAnimationLeft;

    private Settings settings;
	public Vector2 velocity;
	private	Renderer rend;
	private Light playerLight;

    private Vector2 touchOrigin = -Vector2.one;


    private Color getRandomColor(bool newColor = true) {
		int colorIndex = Random.Range ((int)0, colors.Length);

		if (newColor) {
			Color oldColor = rend.material.color;
			if (colors [colorIndex] == oldColor) {
				colorIndex = (colorIndex + 1) % colors.Length;
			}
		}

		return colors [colorIndex];
	}

	private void setColor (Color color,bool material = true, bool light = true, bool emmision = true){
		if(material)
			rend.material.color = color;

		if(light)
			playerLight.color = color;

		if (emmision) {
			rend.material.SetColor ("_EmissionColor", color * 0.5f);
		}

        currColor = color;
	}

	private void dash() {
        if (mana <= 0)
        {
            mana = -1;
            return;
        }
		walls.GetComponent<Walls> ().speed += dashSpeedIncrease;
		dashing = true;

		dashTimeLeft = dashLength;
		RenderSettings.ambientLight = Color.white;
        mana--;
    }

	private void checkDash() {
		dashTimeLeft -= Time.deltaTime;
		float clr = Mathf.Sin( dashTimeLeft / dashLength * Mathf.PI) * 1.3f;
		if(dashing)
			RenderSettings.ambientLight = new Color (clr, clr, clr);
		if ( dashTimeLeft < 0 && dashing)
		{
			dashing = false;
			walls.GetComponent<Walls> ().speed -= dashSpeedIncrease;
			velocity.y = 0;
			RenderSettings.ambientLight = Color.black;
		}
	}

    public void endGame()
    {
        if (end)
            return;

        end = true;

        if (commands.training)
            return;

        endAnimationLeft = endAnimationLength;
        RenderSettings.ambientLight = Color.black;
        walls.GetComponent<Walls>().speed = 0;
    }

    private void checkEnd()
    {
        if (!end)
            return;

        if (commands.training)
        {
            trainingReset();
            if (!win)
                walls.GetComponent<Walls>().writeSocket("{\"died\":" + "\"true\"}");
            win = false;
            return;
        }

        endAnimationLeft -= Time.deltaTime;
        float clr = Mathf.Sin(endAnimationLeft / endAnimationLength * Mathf.PI * 8)*.2f + .9f;
        if (endAnimationLeft < 0)
        {
            transform.Translate(0, -10f * Time.deltaTime, 0);
        }
        else
        {
            setColor(new Color(clr, 0.7f, .7f));
        }

        if (transform.position.y < -10)
        {
            Application.LoadLevel(Application.loadedLevel);
        }
    }

    void init()
    {
        velocity = new Vector2(0, 0);
        setColor(getRandomColor());
        transform.position = new Vector3(-4f, 0f, 0f);
        if (dashing)
        {
            dashing = false;
            walls.GetComponent<Walls>().speed -= dashSpeedIncrease;
            velocity.y = 0;
            RenderSettings.ambientLight = Color.black;
            dashTimeLeft = 0;
        }
        end = false;
        mana = 3;
    }

    void trainingReset()
    {
        init();
        Walls wallScript = walls.GetComponent<Walls>();
        wallScript.init();
    }

    void checkFinish()
    {
        Wall wallScript = walls.GetComponent<Walls>().Wall.GetComponent<Wall>();
        if (wallScript.getPassed() >= 15)
        {
            walls.GetComponent<Walls>().writeSocket("{\"win\":" + "\"true\"}");
            win = true;
            endGame();
        }
    }

	void Start () {
        Application.runInBackground = true;

        settings = settingsObject.GetComponent<Settings> ();
		playerLight = GetComponent<Light> ();
		rend = GetComponent<Renderer>();
		rend.enabled = true;

        init();		

		top = bounds.transform.GetChild (0).transform.position.y;
		bot = bounds.transform.GetChild (1).transform.position.y;		

        commands = AI.GetComponent<NetworkScript>();
	}
	
	// Update is called once per frame
	void Update () {
        bool jumpKey = false;
        bool dashKey = false;
        #if UNITY_STANDALONE || UNITY_WEBPLAYER
            jumpKey = Input.GetButtonDown("Jump") && enableInput;
		    dashKey = Input.GetButtonDown("Fire1") && enableInput;
#else
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];
            if (myTouch.phase == TouchPhase.Began)
            {
                if(myTouch.position.x < Screen.width/2)
                {
                    jumpKey = enableInput;
                }
                else
                {
                    dashKey = enableInput;
                }
            }
        }
#endif

        if (commands.AIjump)
            jumpKey = true;

        if (commands.AIdash)
            dashKey = true;

        if (jumpKey)
		{
			velocity.y = jumpPower;
			setColor (getRandomColor());
            commands.AIjump = false;
		}

		if (dashKey)
		{
			if (!dashing)
				dash();
            commands.AIdash = false;
		}

		checkDash();
        checkFinish();
        checkEnd();

        if (transform.position.y < bot && !end)
			transform.position = transform.position = new Vector3 (transform.position.x, top, transform.position.z);
		
		else if (transform.position.y > top)
			transform.position = new Vector3 (transform.position.x, bot, transform.position.z);
		

		if (Mathf.Abs(velocity.y) <= maxSpeed * Time.deltaTime)
			velocity.y -= settings.gravity * Time.deltaTime;
		else
			velocity.y = Mathf.Sign(velocity.y) * maxSpeed * Time.deltaTime;

		if (!dashing && !end) {
			transform.Translate (velocity.x, velocity.y * Time.deltaTime, 0);
		}

        walls.GetComponent<Walls>().writeSocket("{\"mana\":" + "\"" + mana + "\"}");
        if (mana < 0)
            mana = 0;
        Debug.Log("{\"mana\":" + "\"" + mana + "\"}");
    }
}
