﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

	public float Width = 2f;
	public float Depth = 2f;
	public float InitGap = 4f;

	public GameObject Bounds;

    private float roof;
	private float floor;
	private float gapTop;

	private float speed;

    private Walls parent;
    

    [HideInInspector]
    public static int passedNo = 0;
    public bool passed = false;

    public void ressetPassed()
    {
        passedNo = 0;
    }

    public int getPassed()
    {
        return passedNo;
    }

    void setTop(GameObject topWall){
		topWall.SetActive (true);
		float topWallHeight = roof - gapTop;
		float topWallPos = roof - topWallHeight/2;
		topWall.transform.parent = this.transform;
		topWall.transform.localScale = new Vector3(Width, topWallHeight, Depth);
		topWall.transform.position = new Vector3 (topWall.transform.parent.position.x, topWallPos, topWall.transform.parent.position.z);
		topWall.name = "topWall";
	}

	void setBot(GameObject botWall){
		botWall.SetActive (true);
		float botWallHeight = gapTop - InitGap - floor;
		float botWallPos = gapTop - InitGap - botWallHeight/2;
		botWall.transform.parent = this.transform;
		botWall.transform.localScale = new Vector3(Width, botWallHeight, Depth);
		botWall.transform.position = new Vector3 (botWall.transform.parent.position.x, botWallPos, botWall.transform.parent.position.z);
		botWall.name = "botWall";
	}

    void setTarget(GameObject target, GameObject topWall, GameObject botWall)
    {
        GameObject refObj;
        if (Random.value > .5f)
        {
            refObj = topWall;
        }
        else
        {
            refObj = botWall;
        }

        float minHeight = parent.MinHeight;
        target.SetActive(true);        
        target.transform.parent = this.transform;
        target.transform.localScale = new Vector3(minHeight, minHeight, minHeight);
        target.transform.position = new Vector3(refObj.transform.position.x - refObj.transform.localScale.x/2, refObj.transform.position.y, refObj.transform.position.z);
        target.name = "target_" + refObj.name;        
    }

    void createNewWall(){
		float minHeight = parent.MinHeight;

		// POSITIONING
		roof = Bounds.transform.GetChild(0).transform.position.y;
		floor = Bounds.transform.GetChild(1).transform.position.y;

		gapTop = Random.value * (roof - floor - InitGap - 2*minHeight) + floor + InitGap + minHeight;

		GameObject topWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
		setTop (topWall);

		GameObject botWall = GameObject.CreatePrimitive(PrimitiveType.Cube);
		setBot (botWall);

        GameObject target = GameObject.CreatePrimitive(PrimitiveType.Cube);
        setTarget(target, topWall, botWall);

        // COLORS
        int colorIndex = Random.Range ((int)0, parent.colors.Length);

		topWall.GetComponent<Renderer>().material.color = parent.colors[colorIndex];
		botWall.GetComponent<Renderer>().material.color = parent.colors[colorIndex];
        target.GetComponent<Renderer>().material.color = Color.white;

        topWall.AddComponent<BoxCollider>();
		botWall.AddComponent<BoxCollider>();
        target.AddComponent<BoxCollider>();
    }

    void Start () {
		parent = transform.parent.GetComponent<Walls>();
		createNewWall ();
    }
	
	void Update () {
        GameObject p = GameObject.Find("Player");
        Player player = p.GetComponent<Player>();

        if (player.enableInput)
        {
			transform.Translate(-parent.speed * Time.deltaTime, 0, 0);

            if (transform.position.x < player.transform.position.x && !passed)
            {
                passed = true;
                passedNo++;
                parent.passedWall = true;
                Debug.Log(passedNo);
            }

            if (transform.position.x < -40)
            {
                passed = false;
                Transform tmp = transform.Find("target_topWall");
                if (tmp != null)
                    Destroy(tmp.gameObject);
                tmp = transform.Find("target_botWall");
                if (tmp != null)
                    Destroy(tmp.gameObject);


                GameObject topWall = transform.Find ("topWall").gameObject;
				GameObject botWall = transform.Find ("botWall").gameObject;
                GameObject target = GameObject.CreatePrimitive(PrimitiveType.Cube);
                
                setTop (topWall);
				setBot (botWall);
                setTarget(target, topWall, botWall);

                int colorIndex = Random.Range ((int)0, parent.colors.Length);

				topWall.GetComponent<Renderer>().material.color = parent.colors[colorIndex];
				botWall.GetComponent<Renderer>().material.color = parent.colors[colorIndex];
                target.GetComponent<Renderer>().material.color = Color.white;

                target.AddComponent<BoxCollider>();

                Vector3 newPos = transform.position;
                newPos.x = transform.position.x + parent.WallCount * parent.InitGap;
                transform.position = newPos;
            }            
        }
	}
}
