﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasColor : MonoBehaviour {

    public GameObject Player;
	
	void Update () {
        Button[] buttons = this.GetComponentsInChildren<Button>();

       foreach(Button b in buttons)
        {
            ColorBlock colorVar = b.colors;
            Color playerColor = Player.GetComponent<Player>().currColor;
            playerColor.a = .6f;
            if (colorVar.highlightedColor != playerColor)
            {
                colorVar.highlightedColor = playerColor;
                b.colors = colorVar;
            }
        }
    }
}
