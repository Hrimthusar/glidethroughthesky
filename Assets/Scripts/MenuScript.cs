﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour {

    private bool AI = false;
    GameObject AICanvas;

    public void Start()
    {
        AICanvas = GameObject.Find("AICanvas");
        ShowHideAIMenu(false);
    }

    public void ShowHideAIMenu(bool show)
    {
        AICanvas.gameObject.SetActive(show);
    }

	public void StartGame()
    {
        EnableInput();
        HideMenu();
        if (AI)
            ShowHideAIMenu(true);
    }

    public void enableAI()
    {
        AI = true;
    }

    public void EnableInput()
    {
        GameObject p = GameObject.Find("Player");
        Player player = p.GetComponent<Player>();

        player.enableInput = true;
    }

    public void HideMenu()
    {
        GameObject canvas = GameObject.Find("Canvas");

        canvas.gameObject.SetActive(false);
    }

    public void ExitGame()
    {
        #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
        #else
                 Application.Quit();
        #endif
    }

    public void RestartGame()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
}
