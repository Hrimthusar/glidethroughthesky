#!/usr/bin/env python
from __future__ import print_function

import argparse
import sys
import random
import numpy as np
from collections import deque

import json
import socket

from keras.initializers import normal, identity
from keras.models import model_from_json
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.optimizers import SGD , Adam
import tensorflow as tf

ACTIONS = 3
GAMMA = 0.99
OBSERVATION = 1000
EXPLORE = 3000000.
FINAL_EPSILON = 0.0001
INITIAL_EPSILON = 0.1
REPLAY_MEMORY = 50000
BATCH = 32
LEARNING_RATE = 1e-4

### init values
features = {
    "targetYDistance": 25,
    "midYDistance": 25,
    "distance": 25,
    "vY": 0,
    "mana": 3
}

passedWall = False
dashTarget = False
died = False
win = False

passedWalls = 0

### socket connection globals
client = None
address = None

def buildmodel():
    global features
    model = Sequential()
    model.add(Dense(units = 32, input_dim = len(features), activation = 'relu'))
    model.add(Dense(units = ACTIONS, activation = 'softmax'))

    adam = Adam(lr=LEARNING_RATE)
    model.compile(loss='mse',optimizer=adam)
    model.summary()
    return model

def standardize(features):
    f = features.copy()
    f["mana"] = f["mana"]/10
    f["distance"] = (f["distance"])/25
    f["targetYDistance"] = (f["targetYDistance"]-8)/16
    f["midYDistance"] = (f["midYDistance"]-8)/16
    f["vY"] = (f["vY"]+20)/40

    #print(json.dumps(f))
    return f

def updateFeatures(dataMerge, died, win, dashTarget, passedWall):
    global client
    size = 8192

    loadedData = client.recv(size).decode("utf-8")
    endIndex = loadedData[:loadedData.find('}')]
    dataMerge += loadedData

    if endIndex == -1:
        return

    try:
        data = json.loads(dataMerge[:dataMerge.find('}')+1])
        dataMerge = ""

    except ValueError:
        return;


    if "targetYDistance" in data:
        features["targetYDistance"] = float(data["targetYDistance"])

    if "midYDistance" in data:
        features["midYDistance"] = float(data["midYDistance"])

    if "distance" in data:
        features["distance"] = float(data["distance"])

    if "mana" in data:
        features["mana"] = float(data["mana"])

    if "vY" in data:
        features["vY"] = float(data["vY"])



    if "died" in data:
        died = True

    if "win" in data:
        win = True

    if "passedWall" in data:
        passedWall = True

    if "dashTarget" in data:
        dashTarget = True

    return died, win, dashTarget, passedWall

def waitForFeatures():
    oldFeatures = features

    died = False
    win = False
    dashTarget = False
    passedWall = False
    global passedWalls

    oldFeatures = features
    while True: # waiting for features update from client
        dataMerge = ""
        died, win, dashTarget, passedWall = updateFeatures(dataMerge, died, win, dashTarget, passedWall)
        for key, value in oldFeatures.items():
            if features[key] == value:
                continue
        break

    terminal = False
    reward = 0

    if passedWall:
        print("passedWall")
        reward = 1
        passedWalls += 1

    if dashTarget:
        print("dashTarget")
        reward = 2

    ### More complex rewards
    # else:
    #     if oldFeatures["mana"] > features["mana"] and dashTarget == False:
    #         reward = -2
    #     if oldFeatures["mana"] == 1 and features["mana"] == 0:
    #         reward = -5

    # if oldFeatures["mana"] == 1 and features["mana"] == 0:
    #     reward = -2
    #
    # if features["mana"] == -1:
    #     print("NE VALJA")
    #     reward = -10

    if died:
        print("------------ DIED ------------")
        ### Earning reward as passing obstacles
        ### reward -1 would be redudant
        # reward = -1 - abs(oldFeatures["midYDistance"]) - features["mana"] + passedWalls
        # if reward > -1: reward = -1
        terminal = True

    if win:
        print("  **   ------------ --- ------------")
        print(" **    ------------ WIN ------------")
        print("  **   ------------ --- ------------")

    if died or win:
        with open("log.txt", "a") as outfile:
            outfile.write(str(passedWalls) + "\r\n")
        passedWalls = 0

    #print ("\t" + json.dumps(features))
    if reward != 0:
        print(reward)
    return standardize(features), reward, terminal

def sendAction(a_t):
    global client

    if a_t[1] == 1:
        client.send(("dash\n").encode())
    if a_t[2] == 1:
        client.send(("jump\n").encode())

def trainNetwork(model,args):
    ### Store the previous observations in replay memory
    D = deque()

    ### Init action
    do_nothing = np.zeros(ACTIONS)
    do_nothing[0] = 1
    sendAction(do_nothing)
    s_t, r_0, terminal = waitForFeatures()

    ### Observing for making set big enough to sample from
    OBSERVE = OBSERVATION
    if args['mode'] == 'Load':
        epsilon = FINAL_EPSILON
        # epsilon = ... ### Setting value manually if learning is interuped
        # epsilon = 0.05
        print ("Now we load weight")
        model.load_weights("model.h5")
        adam = Adam(lr=LEARNING_RATE)
        model.compile(loss='mse',optimizer=adam)
        print ("Weight load successfully")
    else:                       #We go to training mode
        epsilon = INITIAL_EPSILON

    t = 0
    while (True):
        loss = 0
        Q_sa = 0
        action_index = 0
        r_t = 0
        a_t = np.zeros([ACTIONS])

        #Choose an action epsilon greedy
        if random.random() <= epsilon:
            action_index = random.randrange(ACTIONS)
            a_t[action_index] = 1
            print("----------Random Action: ", a_t , " ---------- " , epsilon)
            ### Epsilon is printed for storing it if
        else:
            p = np.fromiter(s_t.values(), float).reshape((1, len(features)))
            q = model.predict(p)
            max_Q = np.argmax(q)
            action_index = max_Q
            a_t[max_Q] = 1
            #print(a_t)

        #Epsilon reduction
        if epsilon > FINAL_EPSILON and t > OBSERVE:
            epsilon -= (INITIAL_EPSILON - FINAL_EPSILON) / EXPLORE

        old_features = features

        ### sendAction and waitForFeatures is (s,a) -> st1 "function"
        sendAction(a_t)
        s_t1, r_t, terminal = waitForFeatures()

        D.append((np.fromiter(old_features.values(), float).reshape((1, len(features))), action_index, r_t, (np.fromiter(s_t1.values(), float).reshape((1, len(features)))), terminal))
        if len(D) > REPLAY_MEMORY:
            D.popleft()

        #Only train if done observing
        if t > OBSERVE:
            #sample a minibatch to train on
            minibatch = random.sample(D, BATCH)

            state_t, action_t, reward_t, state_t1, terminal = zip(*minibatch)
            state_t = np.concatenate(state_t)
            state_t1 = np.concatenate(state_t1)
            targets = model.predict(state_t)
            Q_sa = model.predict(state_t1)
            targets[range(BATCH), action_t] = reward_t + GAMMA*np.max(Q_sa, axis=1)*np.invert(terminal)

            loss += model.train_on_batch(state_t, targets)

        s_t = s_t1
        t = t + 1

        # Saving model
        if t % 10000 == 0:
            print("Now we save model")
            model.save_weights("model.h5", overwrite=True)
            with open("model.json", "w") as outfile:
                json.dump(model.to_json(), outfile)

        # print info
        state = ""
        if t <= OBSERVE:
            state = "observe"
        elif t > OBSERVE and t <= OBSERVE + EXPLORE:
            state = "explore"
        else:
            state = "train"

        #print("TIMESTEP", t, "/ STATE", state, \
        #    "/ EPSILON", epsilon, "/ ACTION", action_index, "/ REWARD", r_t, \
        #    "/ Q_MAX " , np.max(Q_sa), "/ Loss ", loss)

    print("Episode finished!")
    print("************************")

def main():
    parser = argparse.ArgumentParser(description='Description of your program')
    parser.add_argument('-m','--mode', help='Train / Load', required=False)
    args = vars(parser.parse_args())

    model = buildmodel()

    ### Setting socket
    host = ''
    port = 50000
    backlog = 5
    size = 4096
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host,port))
    s.listen(backlog)

    global client
    global address

    client, address = s.accept()

    print("Client connected.")
    client.send(("Hello!\n").encode())

    trainNetwork(model,args)

if __name__ == "__main__":
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)
    from keras import backend as K
    K.set_session(sess)
    main()
